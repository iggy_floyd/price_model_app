#!/bin/bash

docker build -t develop_price_model_app -f Dockerfile.development .
if [  $? -eq 0 ]
then
docker run --rm  -p 9001:9000  -v $(pwd)/../:/go/src/bitbucket/iggy_floyd/ develop_price_model_app 
else
exit 1
fi

