#!/bin/bash 

RELEASE_APP=v0.1
RUNMODE_APP=dev
[[ -n $1 ]] && RELEASE_APP=$1
[[ -n $2 ]] && RUNMODE_APP=$2

rm  build.tar.gz
docker build -t build_price_model_app -f Dockerfile.build  .
if [  $? -eq 0 ]
then
docker run --rm   -e RELEASE_APP=$RELEASE_APP -e RUNMODE_APP=$RUNMODE_APP build_price_model_app > build.tar.gz
docker build -t price_model_app -f Dockerfile.dist .
docker rmi build_price_model_app
rm build.tar.gz
else
exit 1
fi


