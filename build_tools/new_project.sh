#!/bin/bash

docker rmi new_price_model_app
docker build -t new_price_model_app -f Dockerfile.new_project .
if [  $? -eq 0 ]
then
 docker run --rm   -v $(pwd):/go/src/bitbucket/iggy_floyd/ new_price_model_app
 docker rmi new_price_model_app
else 
exit 1
fi
