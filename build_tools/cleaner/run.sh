#!/bin/bash

#set -e

#docker login -u pieltestuser -p mkakogalb47 edatahub.dynu.com:5000 


while true
do
	echo "checking containers..."
	docker ps -a
	docker images
	echo "removing..."
        docker ps --no-trunc -aqf "status=exited" | xargs docker rm
        docker images --no-trunc -aqf "dangling=true" | xargs docker rmi
        echo "after removal..."
        docker ps -a
        docker images
	echo "sleeping..."
	sleep 180
done
