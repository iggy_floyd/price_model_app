#!/bin/bash

docker-compose stop
docker-compose rm -f
docker rmi develop_price_model_db
docker rmi develop_price_model_app
docker volume rm price_model_dbdata
rm docker-compose.yml
