#!/bin/bash

docker build -t develop_price_model_app -f Dockerfile.development .
cd db
docker build -t develop_price_model_db -f Dockerfile.development .
cd -
docker volume create --name=price_model_dbdata

cat <<EOF > docker-compose.yml
db:
    container_name: develop_price_model_db
    image: develop_price_model_db
    env_file: .env
    tty: true
    volumes:
    - price_model_dbdata:/var/lib/postgresql/data
app:
    container_name: develop_price_model_app
    image: develop_price_model_app
    ports:
    - 9001:9000
    env_file: .env
    tty: true
    links:
    - db:db
    volumes:
    - `pwd`:/go/src/bitbucket/iggy_floyd/price_model_app
EOF

docker-compose up -d

#if [  $? -eq 0 ]
#then
#docker run --rm  -p 9001:9000  -v $(pwd):/go/src/bitbucket/iggy_floyd/price_model_app develop_price_model_app 
#else
#exit 1
#fi

